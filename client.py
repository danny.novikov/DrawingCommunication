##############################################################################
# FILE: ex12.py
# WRITERS: Daniel Novikov, danielnov90, 302549902 ;
#          Tom Nissim, tomn96, 208307470
# EXERCISE: intro2cs ex12 2015-2016
# DESCRIPTION: The file operates a painting game app, and communicates with
# other players through the server.
##############################################################################


##############################################################################
# Imports:
##############################################################################

from tkinter import *
from sys import argv
from select import select
import socket


##############################################################################
# Constants:
##############################################################################

INITIAL_COLOR = "blue"
INITIAL_SHAPE = "line"
HELP_MESSAGE = "Choose a color from the color menu, and a shape.\nThen," \
               " click twice on the canvas (or 3 times for triangle) to" \
               " create the shape. "
ERROR_NAME_MSG = "Invalid username or group name.\nplease enter a name " \
                 "that contains numbers or letters (or both)"
ERROR_PORT_MSG = "The port is not valid. please enter a numerical expression"

READ_LIMIT = 1024
READ_SOCK = 0
MSG_TYPE = 0
MSG_CONTENT = 1
X_COORD = 0
Y_COORD = 1
TRIANGLE_COORDS_AMOUNT = 3
OVAL_LINE_RECT_COORDS_AMOUNT = 2
DEFAULT_LINE_WIDTH = 3
NAME_COORDS = 0
LEAVE_MSG = "leave\n"
ONLINE_FRIENDS_TITLE = "Online friends:"
STARTING_INDEX = 2
RECTANGLE_FILE = "rectangle.gif"
LINE_FILE = "line.gif"
OVAL_FILE = "circle.gif"
TRIANGLE_FILE = "triangle.gif"

WRONG_PARAMETER_NUMBER_MSG = "Wrong number of parameters. The correct " \
                             "usage is:\nclient.py " \
                             "<host_ip><port><user_name><group_name>"
RIGHT_PARAMETER_NUM = 5
IP_ADDRESS_INDEX = 1
PORT_INDEX = 2
USER_NAME_INDEX = 3
GROUP_NAME_INDEX = 4


class Painter:
    """ The Painter class is a class which operates the painting app. In
    addition, it's responsible for communicating with the server.
    In the Painter app, you can draw different shapes with different colors,
    see your group members, and even ask for help. Once few clients are in the
    same group, they can watch each others drawings on a common canvas.
    """

    def __init__(self, parent, user_name, group_name, channel):
        """ This is the constructor of the Painter class. The constructor gets
        it's parameters from the sys.argv class.
        The constructor contains many different widgets (such as: buttons,
        labels, canvas and scrollbar) which is shown in the app.
        :param parent: The root of the tkinter app. The whole app is based on
         the root.
        :param user_name: The name of the user. which will be shown in app's
        title and in the friends list.
        :param group_name: The name of the group which the client will be in.
        :param channel: A socket which through the client connects and
        communicating with the server.
        :return: None
        """
        self.__parent = parent
        self.__user_name = user_name
        self.__group_name = group_name
        self.__channel = channel

        self.get_parent().title(self.__user_name)

        self.__current_color = INITIAL_COLOR
        self.__current_shape = INITIAL_SHAPE

        self.__menu_bar = Menu(self.get_parent())
        self.__painting_menu_frame = Frame(self.get_parent())
        self.__shapes_label = Label(self.__painting_menu_frame,
                                    text='Current shape: ' +
                                         self.__current_shape+'    ')

        self.__scrollbar = Scrollbar(self.get_parent())
        self.__listbox = Listbox(self.get_parent(),
                                 yscrollcommand=self.__scrollbar.set,
                                 background="white")
        self.__listbox.insert(END, self.__group_name)
        self.__listbox.insert(END, ONLINE_FRIENDS_TITLE)

        #  default height and width of canvas as asked on exercise:
        self.__canvas = Canvas(self.get_parent(), height=500, width=500,
                               background="white")

        self.__online_friends = []
        self.__mouse_coordinates = []

        self.__join_game()
        self.__get_data()
        self.__create_help_menu()

        self.__load_images()
        self.__create_painting_menu()
        self.__create_friends_bar()
        self.__create_canvas()

        self.get_parent().protocol("WM_DELETE_WINDOW",
                                   self.__close_window_handler)

        self.get_parent().resizable(0, 0)  # the 0 make window's size constant

    def get_parent(self):
        """  This method acts as a getter. In addition,
         it's used in this program
        :return: The root of the tkinter app. The whole app is based on
         the root.
        """
        return self.__parent

    def __data_to_list(self, decoded_data):
        """ A side function of __get_data() method. It get's a decoded message
        as an input, and returns a list of lists which contains information
        from the server.
        :param decoded_data: A string. The different information pieces are
        split by '\n', and each piece is split by ';'.
        :return: A list of lists, each list is a contains a whole information
        piece.
        """
        for index in range(len(decoded_data)-1, -1, -1):
            if decoded_data[index] == '\n':
                filtered_decoded_data = decoded_data[:index]
                initial_list = filtered_decoded_data.split('\n')
                final_list = [message.split(';') for message in initial_list]

                return final_list

        return []

    def __join_friend(self, friend_name):
        """ A side function of __get_data() method. If the content of the
        message has information about new friend which was added to client's
        group. The function inserts him to the listbox, and updates our
        friends list attribute.
        :param friend_name: A string which contains the name of our new group
        friend.
        :return: None
        """
        self.__listbox.insert(END, friend_name)
        self.__online_friends.append(friend_name)

    def __draw_other_shapes(self, msg_lst):
        """ A side function of __get_data() method. The function gets the info
        from server (which related to shapes). It uses the info (shape type,
        coordinates, user name, and color) to draw these shapes on the canvas.
        :param msg_lst: A list of lists. each list contains a few parameters:
        user_name, shape type, the coordinates of the shape, and shape color.
        :return: None
        """
        user_name, shape, coords, color = msg_lst[MSG_CONTENT:]
        coords_tuple = tuple(coords.split(','))

        if shape == "line":
            self.__canvas.create_line(coords_tuple, fill=color, width=3)
        elif shape == "rectangle":
            self.__canvas.create_rectangle(coords_tuple, fill=color)
        elif shape == "oval":
            self.__canvas.create_oval(coords_tuple, fill=color)
        elif shape == "triangle":
            self.__canvas.create_polygon(coords_tuple, fill=color)

        self.__canvas.create_text(coords_tuple[X_COORD:Y_COORD+1],
                                  text=user_name)

    def __friend_leave(self, msg_list):
        """ A side function of __get_data() method. If one of our group's
        friends left the app, The function gets the data from the server,
        and updating the listbox widget in my online_friends attribute.
        :param msg_list: A list of strings, the 2nd element contains the name
        of the friend which left our app group.
        :return: None
        """
        friend_name = msg_list[MSG_CONTENT]
        index = self.__online_friends.index(friend_name)
        self.__listbox.delete(index + STARTING_INDEX)
        self.__online_friends.remove(friend_name)

    def __add_initial_friends(self, msg_lst):
        """ A side function of __get_data() method. Once the client joined a
        a group with existing clients, he gets a list of his group friends.
        The function updates the online_friends attribute and the listbox
        widget.
        :param msg_lst: A list of strings. The 2nd element of the list
        contains the names of the existing group friends of the client.
        :return: None
        """
        users = msg_lst[1].split(',')

        for username in users:
            self.__listbox.insert(END, username)
            self.__online_friends.append(username)

    def __error_msg(self, error_msg):
        """ A side function of __get_data() method. Once an error exception
        was raised, Using the data information which we get from the server,
        a new window will appear. The window contains the error content.
        :param error_msg: A string. which explains which error had accrued.
        :return: None
        """
        error_root = Toplevel()
        error_root.title("External error")
        frame1 = Frame(error_root, width=300, height=50)
        frame1.pack()
        help_label = Label(error_root, text=error_msg)
        help_label.pack()
        frame2 = Frame(error_root, width=300, height=50)
        frame2.pack()

    def __get_data(self):
        """ This method is a main method which gets the data from the server.
        It converts the data into a readable data list of lists.
        Using the content of the data, It determines the type of the message.
        Once the message type is determined, the method sends the information
        to a relevant side functions which update client's attributes and
        updates the GUI app according to the information.
        :return: None
        """
        decoded_data = ''

        readable_sockets = select([self.__channel], [], [], 0.01)[READ_SOCK]
        while len(readable_sockets) > 0:
            for sock in readable_sockets:
                if sock == self.__channel:
                    coded_data = sock.recv(READ_LIMIT)
                    decoded_data += coded_data.decode()
                    readable_sockets = \
                        select([self.__channel], [], [], 0.01)[READ_SOCK]

        data_list = self.__data_to_list(decoded_data)

        for msg_lst in data_list:
            if msg_lst[MSG_TYPE] == "join":
                self.__join_friend(msg_lst[MSG_CONTENT])
            elif msg_lst[MSG_TYPE] == "shape":
                self.__draw_other_shapes(msg_lst)
            elif msg_lst[MSG_TYPE] == "leave":
                self.__friend_leave(msg_lst)
            elif msg_lst[MSG_TYPE] == "users":
                self.__add_initial_friends(msg_lst)
            elif msg_lst[MSG_TYPE] == "error":
                self.__error_msg(msg_lst[MSG_CONTENT])

        self.get_parent().after(100, self.__get_data)

    def __join_game(self):
        """ The method is responsible for sending a joining message to the
        server once the client has joined a group. The client is sending an
        encoded message to the server, which contains client's name.
        :return: None
        """
        join_msg = "join;" + self.__user_name + ';' + self.__group_name + '\n'
        self.__channel.sendall(bytes(join_msg, encoding='utf8'))

    def __create_help_menu(self):
        """ The method is responsible for creating a specific command for the
        help menu and packing  the help menu.
        :return: None
        """
        self.__menu_bar.add_command(label="Help", command=self.__help_handler)
        self.get_parent().config(menu=self.__menu_bar)

    def __help_handler(self):
        """ A handler function of __create_help_menu method. The function is
        activated, when the client clicks on the 'help' Menu. Once the help
        menu is clicked, a new window will appear. The new help window
        contains information about how to play the game.
        :return: None
        """
        help_root = Toplevel()
        help_root.title("HELP")
        frame1 = Frame(help_root, width=300, height=50)
        frame1.pack()
        help_label = Label(help_root, text=HELP_MESSAGE)
        help_label.pack()
        frame2 = Frame(help_root, width=300, height=50)
        frame2.pack()

    def __create_painting_menu(self):
        """ This method creates a painting menu in the Painter's gui.
        The painting menu has colors options menu, and shapes buttons. The
        colors menu is used to pick a color to draw a shape which was picked
        by the shapes buttons.
        :return: None
        """
        # creates the frame which the painting menu is located in:
        self.__painting_menu_frame.pack(fill=X)

        color_label = Label(self.__painting_menu_frame, text="Color:  ")
        color_label.pack(side=LEFT)

        self.__create_color_menu()  # creates the colors options menu

        self.__create_shape_buttons()
        self.__shapes_label.pack(side=RIGHT)

    def __create_color_menu(self):
        """ This method creates a colors options menu, and packs it into the
        frame of the painting menu.
        :return: None
        """
        # The variable holding the color picked in the option menu:
        variable = StringVar(self.__painting_menu_frame)
        variable.set(INITIAL_COLOR)  # We picked initial color for the users.

        # An option menu with the colors described in the exercise as options:
        colors_option_menu = OptionMenu(self.__painting_menu_frame, variable,
                                        "blue", "red", "green", "yellow",
                                        "black", "violet", "orange",
                                        command=self.__color_menu_handler)
        colors_option_menu.pack(side=LEFT)

    def __color_menu_handler(self, value):
        """ A handler to the event of picking a color in the colors options
        menu.
        :param value: A string, representing the current value that is picked
        in the options menu. i.e The color that the user picked using the
        option menu.
        :return: None
        """
        # set the current color to the color the user picked:
        self.__current_color = value

        # When the user picks a new color, we decided to 'forget' all of the
        # clicks that he/she has made in the past. See README:
        self.__mouse_coordinates = []

    def __load_images(self):
        """ This method loads all the necessary images for the shapes buttons,
        and sets them as attributes of the Painter object so that they can be
        used later.
        :return: None
        """
        self.__rectangle_image = PhotoImage(file=RECTANGLE_FILE)
        self.__triangle_image = PhotoImage(file=TRIANGLE_FILE)
        self.__oval_image = PhotoImage(file=OVAL_FILE)
        self.__line_image = PhotoImage(file=LINE_FILE)

    def __create_shape_buttons(self):
        """ Creates shapes buttons for the painting menu, and packs them into
        their master (The painting menu frame). The shapes buttons display
        the suitable image for each specific button.
        :return: None
        """
        rectangle = Button(self.__painting_menu_frame,
                           image=self.__rectangle_image,
                           command=self.__shape_button_handler("rectangle"))
        rectangle.pack(side=RIGHT)

        triangle = Button(self.__painting_menu_frame,
                          image=self.__triangle_image,
                          command=self.__shape_button_handler("triangle"))
        triangle.pack(side=RIGHT)

        oval = Button(self.__painting_menu_frame,
                      image=self.__oval_image,
                      command=self.__shape_button_handler("oval"))
        oval.pack(side=RIGHT)

        line = Button(self.__painting_menu_frame,
                      image=self.__line_image,
                      command=self.__shape_button_handler("line"))
        line.pack(side=RIGHT)

    def __shape_button_handler(self, shape):
        """ This method takes a shape and returns a function which is the
        shape's button handler for the event of clicking the shape's button.
        Args:
            shape: A string representing the shape's name (the shape suitable
            for the button that has been clicked).

        Returns: A function. The function is the button handler suitable for
        the given shape's button.
        """
        def determine_shape():
            """ This function is a handler of a shape's button click (the
            shape is saved because of the closure feature). It used as the
            command of the button, and it does all of the actions necessary
            when clicking the specific shape button.
            Returns: None

            """
            # set the current shape to the given shape in
            # self.__shape_button_handler which is saved by the closure:
            self.__current_shape = shape

            # When the user picks a new shape, we decided to 'forget' all of
            # the clicks that he/she has made in the past. See README:
            self.__mouse_coordinates = []

            self.__shapes_label.config(text='Current shape: ' +
                                            self.__current_shape+'    ')

        return determine_shape

    def __create_friends_bar(self):
        """ This method adds an 'online friends' box to the left of the
        screen. This box contains the group's name and all of the friends
        which are currently connected to the same group as the user. It also
        has a scroll bar so the user can scroll up and down to see all of
        his online friends.
        Returns: None
        """
        self.__listbox.pack(side=LEFT, fill=BOTH)

        self.__scrollbar.pack(side=LEFT, fill=Y)
        self.__scrollbar.config(command=self.__listbox.yview)

    def __create_canvas(self):
        """ This method creates a canvas in which the user can draw his
        shapes and see the shapes drawn by other friends in his group
        Returns: None

        """
        self.__canvas.pack(side=LEFT)
        # gets the coordinates of a mouse right click on the canvas:
        self.__canvas.bind("<Button-1>", self.__get_click_coords)

    def __get_click_coords(self, event):
        """ This is a handler of the event of mouse right click on the
        canvas. It does all of the necessary actions to this event.

        Args:
            event: An event of the gui, specifically it is a mouse right
            click on the canvas.

        Returns: None

        """
        point = (event.x, event.y)  # clicking coordinates in the canvas
        self.__mouse_coordinates.append(point)
        self.__draw_shape()  # makes the suitable shape if necessary

    def __list_to_tuple(self, lst):
        """ This method takes a list of tuples, each tuple contains two
        numbers representing x and y coordinates of a mouse click. It returns
        one tuple contains all of the values from the given list in the form
        of: (x1,y1,x2,y2,...) , Where xi is the first value and yi is
        the second value of the i tuple in the given list.
        Args:
            lst: A list of tuples in the form of:
            [(x1,y1),(x2,y2),...]

        Returns: A tuple, contains all of the mouse clicks data from the
        given list. It is in the form described above.

        """
        answer_list = []
        for tup in lst:
            answer_list.append(tup[X_COORD])
            answer_list.append(tup[Y_COORD])
        return tuple(answer_list)

    def __tup_string(self, mouse_coordinates):
        """ This method takes a tuple of mouse clicks coordinates as given by
        self.__list_to_tuple (x1, y1, x2, y2, ...) . It returns a
        string of all of the coordinates in the same order separated by
        commas: 'x1,y1,x2,y2,...' .

        Args:
            mouse_coordinates: A tuple of mouse clicks  on the canvas
            coordinates as given by the self.__list_to_tuple method:
            (x1, y1, x2, y2, ...)

        Returns: A string. contains all of the clicks coordinates separated
        by commas: 'x1,y1,x2,y2,...'

        """
        # a string of the given tuple without the parentheses:
        temp_string = str(mouse_coordinates)[1:-1]

        # removes all of the spaces that are located after every comma:
        final_string = temp_string.replace(" ", "")

        return final_string

    def __draw_shape(self):
        """ This method checks if there are enough mouse clicks to draw the
        shape. If there are enough it sends the suitable shape message to
        the server.
        Returns: None

        """
        mouse_coordinates = self.__list_to_tuple(self.__mouse_coordinates)
        msg_coords = self.__tup_string(mouse_coordinates)
        # The suitable shape message as described in the exercise:
        shape_message = "shape" + ';' + self.__current_shape + ';' + \
                        msg_coords + ';' + self.__current_color + '\n'

        if len(self.__mouse_coordinates) == TRIANGLE_COORDS_AMOUNT:

            self.__mouse_coordinates = []  # nullify clicked mouse coords

            # send the suitable message to the server:
            self.__channel.sendall(bytes(shape_message, encoding='utf8'))

        elif len(self.__mouse_coordinates) == OVAL_LINE_RECT_COORDS_AMOUNT \
                and self.__current_shape != "triangle":

            self.__mouse_coordinates = []  # nullify clicked mouse coords

            # send the suitable message to the server:
            self.__channel.sendall(bytes(shape_message, encoding='utf8'))

    def __close_window_handler(self):
        """ This method is a handler to the event of closing the game's
        window (protocol).
        Returns: None

        """
        # send a leave message to the server as described in the exercise:
        self.__channel.sendall(bytes(LEAVE_MSG, encoding='utf8'))
        self.get_parent().quit()  # close the gui's window


def name_valid(name):
    """ This function checks if a given string is a valid name (according to
    the exercise's description), and returns True or False respectively.
    If the name is not valid It pops an error window to the user.
    Args:
        name: A string to be checked if valid

    Returns: True if the name contains only alphabetic or numeric expressions
    or False otherwise.

    """

    if not name.isalnum():  # the name is not valid according to the exercise
        # open an error window to the user:

        error_root = Tk()
        error_root.title("ERROR!")
        frame1 = Frame(error_root, width=300, height=50)
        frame1.pack()
        help_label = Label(error_root, text=ERROR_NAME_MSG)
        help_label.pack()
        frame2 = Frame(error_root, width=300, height=50)
        frame2.pack()
        error_root.mainloop()

        return False

    return True


def port_is_valid(port):
    """ This function checks if a given port is valid. i.e. checks if the
    port is a string of a number. If the port is not valid it prints an error
    message to the user.
    Args:
        port: A string representing a port to be checked

    Returns: True if the port is valid, False otherwise

    """

    if port.isnumeric():
        return True
    else:
        print(ERROR_PORT_MSG)
        return False


def main():
    """ This function is the main function of the program. It checks the
    parameters given by the user, initializing a connection with the server
    and creates the gui for the game.
    Returns: None

    """
    if len(argv) != RIGHT_PARAMETER_NUM:
        print(WRONG_PARAMETER_NUMBER_MSG)
    else:
        IP_address = argv[IP_ADDRESS_INDEX]
        user_name = argv[USER_NAME_INDEX]
        group_name = argv[GROUP_NAME_INDEX]

        if name_valid(user_name) and name_valid(group_name) and \
                port_is_valid(argv[PORT_INDEX]):

            port = int(argv[PORT_INDEX])

            # initializing a connection with the server:
            channel = socket.socket()
            channel.connect((IP_address, port))

            # creates the game's gui:
            root = Tk()
            Painter(root, user_name, group_name, channel)
            root.mainloop()

            # ending communication with the server after finishing the game:
            channel.close()


if __name__ == "__main__":
    main()
